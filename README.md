# Listen_to_events_in_oil_and_gas_industry_in_uganda_using_twitter_api

## Steps

### **Create a directory**

$ mkdir oil_and_gas_tweets 

This directory is the one that will hold all the code used to grab the twitter data


### **Create a virtualenvironment**
A virtual environment is a technique that helps to manage separate installation packages of different projects

i created a virtualenvironment called "Tweepy" using the command below

$ python3 -m venv tweepy

### **Activate the virtual environment**

cd .. into the /.virtualenvs/tweepy/bin and type the command below

$ source activate 

This will activate the virtualenv that you will have created .

### **Go to home directory**

$ cd 

### **Go to the 'oil_and_gas_tweets' directory**
$ cd code/oil_and_gas_tweets

### **Installation**

$ pip install tweepy

### **Authentication**

Tweepy supports application-user and application authentication.

We must register our client application with Twitter

Create a new application inorder to get a consumer key and secret.

You must store these two string values

auth.access_token

auth.access_token_secret

## **create a tweet using Twitter api**

activate your virtual environment
create a new python file to run the tweepy code
NB: Donot name it "tweepy.py"

In the short code , it shows the four common steps to all Tweepy programs
1.import the tweepy package
2.Set the authentication credentials
3.Create a new tweepy.API object
4.Use the api object to the call the Twitter API

## **Solution**

Login in your app: http://dev.twitter.com/apps

In the Settings tab, change the Application type to Read, Write and Access direct messages

In the Reset keys tab, press the Reset button, update the consumer key and secret in your application accordingly.


## **Functionalities of Tweepy**
Tweepy give you an interface to access the Twitter API from the python

A status is a tweet

A friendship is a follow-follower relationship

A favourite is a like

Tweepy's functionality is divided into the following groups

+ OAuth
+ API class
+ Models
+ Cursors
+ Streams

OAuth 
Tweepy provides an OAuthHandler class that one can use to set credentials to be used in all API calls


API class
It has many methods that provide access to Twitter API endpoints . Using these methods , you can access the Twitter API's functionality.

The code below creates a  API object that i can use to invoke various Twitter API methods ,Setting wait_on_rate_limit and wait_on_rate_limit_notify to True makes the API object print a message and wait if the rate limit is exceeded:

<!-- ![create api object][api_object.png] -->

API methods can be grouped into the following categories
+ Methods for usertimelines
+ Methods for tweets
+ Methods for users
+ Methods for followers
+ Methods for your account
+ Methods for likes
+ Methods for blocking users
+ Methods for searches 
+ Methods for trends
+ Methods for streaming

Streaming allows us to actively watch for tweets that match a certain criteria in real time . if there is nothing , then the program will wait 

but we have to use two objects 
Stream object - gets tweets from the Twitter API that match some criteria

stream listener - receives tweets from the stream

## **Models**
Tweepy uses its own model classes to encapsulate the responses from various Twitter API methods .This gives you a convient way to use the results from API operations

The model classes are User ,status , Friendship , SearchResults

This enables me to create concise and understandable code

## **Cursors**
Alot of Twitter APi use pagination to retrurn their results.by default each method returns the first page which contains a few dozen items

Tweepy cursors take away part of the complexity of working with paginated results . Cursors are implemented as a Tweepy class named Cusor .To use a cursor , we select an API method to use to fetch items and the number of items that we want . The cursor object takes care of fetching the various result pages transparently

## **How to make a Twitter bot in Python**
The first bot am going to build is a Follow followers bot
-it automatically follows anyone who follows me

## **Create Config module**
All the bots that we are building will have some common functionality , they will need to authenicate to the Twitter API.

So i can create a resusable Python module containing the logic common to all bots

We can also avoid hard coding them into the source code , making it much more secure


## **Create a django application**

***
## **Setup Django**

## Models in Django application

### **Tweet table**
This is the primary table and it will have the following attributes 

+ id , author_id , text , created_at , source 

+ id-(The unique identifier of the requested Tweet), 

+ author_id-(The unique identifier of the User who posted this Tweet), 

+ text-(The actual UTF-8 text of the Tweet. See twitter-text for details on what characters are currently considered valid), 

+ created_at-(Creation time of the Tweet), 

Because we need to see to know what they texted ,at what time and what device ,did they use.

+ source-(The name of the app the user Tweeted from),


### **User table**
User table contains Twitter user account metadata describing the referenced user ,it has the following attributes 

+ name , username , location , 

+ id , name , username , created_at , verified 

+ id-(The unique identifier of this user),

+ name-(The name of the user, as they’ve defined it on their profile),

+ username-(The Twitter screen name, handle, or alias that this user identifies themselves with),

+ created_at-(The UTC datetime that the user account was created on Twitter,how long someone has been using Twitter)

+ verified-(Indicates if this user is a verified Twitter User)


### **Connecting the two tables** 
Am going to use the Author-id as a primary key inside the Tweet table and Tweep table 

## **Setup the views in Django**

## **Create views** 
A view is a type of web page in a Django application that generally servers a specific function and has a template.

Each view is represented by a python function .Django will choose a view by examining the URL that's requested

## **Css animations**

The animation property in CSS can be used to animate many other CSS properties such as color, height

Each animation needs to be defined with the @keyframes at -rule which is then called an animation property

Each @keyframes at-rule defines what should happen at specific moments during the animation For example 0% is the beginning and 100% at the end .These key frames can be controlled either by the shorthand animation property or its eight sub-properties


## **Create a file to automatically input into the django application**
The file is called "awareness_input.py"

+ open django settings

+ import Django models

+ Authenticate yourself to Twitter

+ Query the Twitter API

+ Input the twitter data inside the Django models

## How to deal with lazy django query sets**
Any lazy iterable will be evaluated immediately if you iterate through it with a for loop. The simplest way is to convert it to a list of tuple: list(queryset)

## Schedule script to enter data Automatically
First thing is to install a python package called schedule

Using crontab

Cron utility runs based on commands specified in a cron table 
Each user can have a cron file , they dnot exist by default but can be created in the /var/spool/cron directory using the crontab -e command

## Deployment to Heroku
***

### Deploying Django

For Jesus's sake for this tutorial 
https://www.youtube.com/watch?v=6DI_7Zja8Zc

### Setup Heroku
$ sudo snap install heroku --classic

### login into Heroku CLI
$ heroku login

### Prepare the app
$ git clone https://github.com/heroku/python-getting-started.git

$ cd python-getting-started

We now have a functioning git repository that contains a simple application, a runtime.txt specifying which Python version will be used, and a requirements.txt, which is used by Python’s dependency manager, Pip.

### Deploy the app

$ heroku create

When you create an app, a git remote (called heroku) is also created and associated with your local git repository.

Heroku generates a random name (in this case serene-caverns-82714) for your app, or you can pass a parameter to specify your own app name.

## Deploy the code
$ git push heroku main

## The application is now deployed. Ensure that at least one instance of the app is running:
$ heroku ps:scale web=1

## one can open the website like

$ heroku open

## Heroku requirements
Git

## Setting up Crontab
$ crontab -l
To check if user has any cron jobs

$ crontab -e 
To select the editor one would want to use

use nano to edit things

## Connect Twillio API to  Django

+ Sign up for a Twilio account

Or activate 

Twilio Sandbox for Whatsapp which allows me to prototype with Whatsapp immediately using a shared phone number without waiting for a dedicated number to be approved by WhatsApp.

To get started , select a number from the available sandbox numbers to activate my sand box


1. Set Up Your Testing Sandbox

To send messages with WhatsApp in production, WhatsApp has to formally approve your account. But, that doesn't mean you have to wait to start building. Twilio Sandbox for WhatsApp lets you test your app in a developer environment.

To begin testing, connect to your sandbox by sending a WhatsApp message from your device to +1 415 523 8886 with code join ring-paragraph. If WhatsApp is installed on this device.

Note: Sandbox is not intended for production usage. Sandbox sessions expire after 3 days.

My Twilio sand box is called +14155238886

2. Send a One-Way WhatsApp Message

Messaging with WhatsApp has a few differences from SMS: to send outbound messages, such as notifications, you must use a pre-approved template from WhatsApp.

## Extracting the data 
```python
import sqlite3
from xlsxwriter.workbook import Workbook
workbook = Workbook('Tweets.xlsx')
worksheet = workbook.add_worksheet()
# Pass in the database path, db.s3db or test.sqlite
conn=sqlite3.connect('db.sqlite3')
c=conn.cursor()
mysel=c.execute("select * from oil_tweet_quest_tweet")
for i, row in enumerate(mysel):
    for j, value in enumerate(row):
        worksheet.write(i, j, value)
workbook.close()
```
## Requirements for extracting the data
The sqlite3 python module comes installed with the django already

The xlsx writer must be installed 

pip install xlsxwriter 

## hiccups 
I was finding issues with hitting the database tables because i was at first reffering to it as "tweet" but its supposed to be "oil_tweet_quest_tweet". You do this by list the tables available in sqlite by entering ,its API and using the command " .tables ;" to list the tables

## Requirements
+ An approved Developer account from Twiiter

+ A bearer token 

+ Python 3.7.9

+ virtualenvironment


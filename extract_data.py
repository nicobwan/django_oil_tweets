import sqlite3
from xlsxwriter.workbook import Workbook
workbook = Workbook('Tweets.xlsx')
worksheet = workbook.add_worksheet()
# Pass in the database path, db.s3db or test.sqlite
conn=sqlite3.connect('db.sqlite3')
c=conn.cursor()
mysel=c.execute("select * from oil_tweet_quest_tweet")
for i, row in enumerate(mysel):
    for j, value in enumerate(row):
        worksheet.write(i, j, value)
workbook.close()
from django.apps import AppConfig


class OilTweetQuestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'oil_tweet_quest'

# Generated by Django 3.2.5 on 2021-07-13 17:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Tweep',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author_id', models.IntegerField()),
                ('name', models.TextField()),
                ('start_date', models.DateTimeField()),
                ('verified', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Tweet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('created_at', models.DateTimeField()),
                ('device', models.TextField()),
                ('Tweep', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='oil_tweet_quest.tweep')),
            ],
        ),
    ]
